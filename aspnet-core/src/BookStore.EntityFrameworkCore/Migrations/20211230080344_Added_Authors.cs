﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace BookStore.Migrations
{
    public partial class Added_Authors : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            //migrationBuilder.CreateTable(
            //    name: "AppAuthors",
            //    columns: table => new
            //    {
            //        Id = table.Column<Guid>(type: "RAW(16)", nullable: false),
            //        Name = table.Column<string>(type: "NVARCHAR2(64)", maxLength: 64, nullable: false),
            //        BirthDate = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
            //        ShortBio = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //        ExtraProperties = table.Column<string>(type: "NVARCHAR2(2000)", nullable: true),
            //        ConcurrencyStamp = table.Column<string>(type: "NVARCHAR2(40)", maxLength: 40, nullable: true),
            //        CreationTime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: false),
            //        CreatorId = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //        LastModificationTime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true),
            //        LastModifierId = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //        IsDeleted = table.Column<bool>(type: "NUMBER(1)", nullable: false, defaultValue: false),
            //        DeleterId = table.Column<Guid>(type: "RAW(16)", nullable: true),
            //        DeletionTime = table.Column<DateTime>(type: "TIMESTAMP(7)", nullable: true)
            //    },
            //    constraints: table =>
            //    {
            //        table.PrimaryKey("PK_AppAuthors", x => x.Id);
            //    });

            //migrationBuilder.CreateIndex(
            //    name: "IX_AppAuthors_Name",
            //    table: "AppAuthors",
            //    column: "Name");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "AppAuthors");
        }
    }
}
