﻿using AutoMapper;
using BookStore.Authors;
using BookStore.Books;
using BookStore.Customers;

namespace BookStore
{
    public class BookStoreApplicationAutoMapperProfile : Profile
    {
        public BookStoreApplicationAutoMapperProfile()
        {
            /* You can configure your AutoMapper mapping configuration here.
             * Alternatively, you can split your mapping configurations
             * into multiple profile classes for a better organization. */
            CreateMap<Book, BookDto>().ReverseMap();
            CreateMap<Author, AuthorDto>();
            CreateMap<Customer, CustomerDto>().ReverseMap();
        }
    }
}
