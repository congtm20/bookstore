﻿using BookStore.Books;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Volo.Abp.Application.Dtos;
using Volo.Abp.Application.Services;
using Volo.Abp.Domain.Repositories;
using Volo.Abp.Linq;

namespace BookStore.Customers
{
    public class CustomerAppService : CrudAppService<
            Customer, //The Book entity
            CustomerDto, //Used to show books
            Guid, //Primary key of the book entity
            PagedAndSortedResultRequestDto, //Used for paging/sorting
            CustomerDto>, //Used to create/update a book
        ICustomerAppService //implement the IBookAppService
    {
        private readonly IRepository<Customer> _customerRepository;
        private readonly IAsyncQueryableExecuter _asyncExecuter;
        public CustomerAppService(IRepository<Customer, Guid> customerRepository, IAsyncQueryableExecuter asyncExecuter) : base(customerRepository)
        {
            _customerRepository = customerRepository;
            _asyncExecuter = asyncExecuter;
        }

        //public override Task<CustomerDto> CreateAsync(CustomerDto input)
        //{
        //    //var queryable = await _customerRepository.GetQueryableAsync();
        //    //var customer = new Customer
        //    return base.CreateAsync(input);
        //}

        //public override Task DeleteAsync(Guid id)
        //{
        //    return base.DeleteAsync(id);
        //}

        //public override async Task<PagedResultDto<CustomerDto>> GetListAsync(PagedAndSortedResultRequestDto input)
        //{
        //    //var queryable = await _customerRepository.GetQueryableAsync();
        //    //var query = queryable
        //    //    .Select(cus => cus.Id)
        //    //    .ToList();
        //    //var customer = await _asyncExecuter.ToListAsync(query);
            
        //    //return PagedResultDto<CustomerDto>(customer);
        //}

        //public override Task<CustomerDto> UpdateAsync(Guid id, CustomerDto input)
        //{
        //    return base.UpdateAsync(id, input);
        //}
        
    }
}
