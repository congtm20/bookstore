﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Volo.Abp.Application.Dtos;

namespace BookStore.Customers
{
    public class CustomerDto : AuditedEntityDto<Guid>
    {
        [Required]
        [StringLength(128)]
        public string First_Name { get; set; }
        [Required]
        [StringLength(128)]
        public string Last_Name { get; set; }
        [Required]
        [EmailAddress]
        [StringLength(128)]
        public string Email { get; set; }
        [Required]
        [StringLength(10)]
        public string Phone { get; set; }
        [Required]
        [DataType(DataType.Date)]
        public DateTime Dob { get; set; }
    }
}
